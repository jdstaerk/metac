<div>
  <p align="center">
    <a href="https://gitlab.com/jdstaerk/metac">
      <img src="/misc/Metac.png" alt="Metac logo" width="300">
    </a>
  </p>

  <h3 align="center">Metac</h3>

  <p align="center">
    A simple css framework for the modern web.
  </p>
  <p align="center">
    <a href="https://metac.now.sh/">Demo</a>
  </p>
</div>

## Getting started
1. Run `npm install`
2. Run `npm build` to build the css files
3. Run `npm test` to test the (s)css
4. Have fun!

### Hot reloading
Use `npm run dev` to start a file watcher and hot reloading server.
